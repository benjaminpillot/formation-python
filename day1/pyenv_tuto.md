# Pyenv small tutorial

``pyenv`` allows for management of Python versions on a same machine as well as installation of virtual environments (virtualenv, conda, etc.)

## Installation

#### Prerequisites
Open a terminal and execute:

```shell
$ sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
xz-utils tk-dev libffi-dev liblzma-dev python-openssl git
```

If you're having trouble later to install older pythons versions (lower than 3.5), changing the openssl lib might help:

```shell
$ sudo apt-get remove libssl-dev
$ sudo apt-get update
$ sudo apt-get install libssl1.0-dev
```

Now, use pyenv-installer in order to install pyenv:

```shell
$ curl https://pyenv.run | bash
```

Then add these lines to your `~/.bashrc`:

```shell 
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
```


## Install Python versions with pyenv

Now you have pyenv installed, let's install some Python version, such as the 3.5 :

```shell
$ pyenv install 3.5.0
```

You may check currently installed pythons versions:

```shell
$ pyenv versions
```

## Uninstall Python versions with pyenv

In a terminal :
```shell
$ pyenv uninstall 3.5.0
```
will remove the 3.5.0 version you installed earlier

## Install virtual environments

Open a terminal and clone the pyenv-virtualenv plugin:
```shell
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
```

### List current virtual environments

Whenever you want to list your virtual environments:
```shell
$ pyenv virtualenvs
```

### Virtualenv

Now you can create virtual environments based on versions you have installed:
```shell
$ pyenv virtualenv 3.7.0 myenv3.7.0
```

Activate it:
```shell
$ pyenv activate myenv3.7.0
```

Deactivate it:
```shell
$ pyenv deactivate
```

And remove it when you do not want to use it anymore:
```shell
$ pyenv virtualenv-delete myenv3.7.0
```

### Anaconda

You may install the last anaconda version using pyenv:
```shell
$ pyenv install anaconda3-5.3.1
```

Set the anaconda version as the current version (it is the version where "conda" exists):
```shell
$ pyenv global anaconda3-5.3.1
```

You can create a new conda environment with specific packages by using the following:
```shell
$ conda create -n myenv python=3.5 numpy ipython scipy geopandas
```

Activate it:
```shell
$ pyenv activate anaconda3-5.3.1/envs/myenv
```

Deactivate it:
```shell
$ pyenv deactivate
```

And remove it:
```shell
$ pyenv virtualenv-delete anaconda3-5.3.1/envs/myenv
```



