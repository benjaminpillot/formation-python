# Programme du jour

## 1. Les opérateurs logiques, l'indexation et les _slices_ ([tutorial](https://framagit.org/benjaminpillot/formation-python/-/blob/master/day3/logical_operator_and_indexing.md))
* Comparaison de valeurs et booléens
* Retourner une ou des valeurs d'une collection à l'aide de l'indexation et des _slices_

## 2. Les structures de contrôle ([tutorial](https://framagit.org/benjaminpillot/formation-python/-/blob/master/day3/python_control_flow.md))
* Les boucles, les conditions, les _comprehension lists_
* Better ask for permission : attraper les erreurs 

## 3. Programmer de façon simple et Claire: le _pythonic_ way of life ([tutorial](https://framagit.org/benjaminpillot/formation-python/-/blob/master/day3/python_programming_ways.md))
* Python possède sa propre syntaxe, ses propres conventions, ses propres _tricks_.
* Au-delà de ça, la philosophie Python s'applique à l'ensemble de la programmation. En clair, évitez les usines à gaz !
* Les décorateurs

## 4. La programmation impérative ([tutorial](https://framagit.org/benjaminpillot/formation-python/-/blob/master/day3/imperative_programming.md))
* La programmation prcédurale: un script, des routines, des fonctions
* La programmation orientée objet: des objets 
