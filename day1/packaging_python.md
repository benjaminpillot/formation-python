# Packaging and uploading Python projects

## Typical files available in a package
* Package
* ``setup.py``
* ``LICENCE``
* ``README.md``

## Creating ``setup.py``
Below an example of ``setup.py`` file for some awesome package:

```python
from setuptools import setup, find_packages

import my_awesome_package

with open("README.md", 'r') as fh:
    long_description = fh.read()

with open("requirements.txt") as req:
    install_req = req.read().splitlines()

setup(name='my_awesome_package',
      version=my_awesome_package.__version__,
      description='An awesome package',
      long_description=long_description,
      long_description_content_type="text/markdown",
      url='http://github.com/awesomepackageteam/my-awesome-package.git',
      author='The awesome package team',
      author_email='awesomepackageteam@mtd.com',
      install_requires=install_req,
      python_requires='>=3',
      license='GNU GPL v3.0',
      packages=find_packages(),
      zip_safe=False)
```
* ``name`` is the _distribution name_ of your package. Must only contain letters and numbers
* ``version`` is the package version
* ``description`` is a short, one-sentence summary of the package
* ``long_description`` is a detailed description of the package. In this case, the long description is loaded from README.md which is a common pattern.

## Generating distribution archives
Once you're good with your package, it's time to upload an archive to the Package Index that can be installed by ``pip``.

Activate your virtual environment:
```shell
$ pyenv activate my_env
```

Make sure you have the latest versions of ``setuptools``:
```shell
(my_env) $ pip install --upgrade setuptools
```

Now, to build your distribution, run the following command from the same directory where ``setup.py`` is located:
```shell
(my_env) $ python setup.py sdist
```

## Uploading the distribution archives

Once you have registered an account on PyPI, you're ready to upload your package. First, open a terminal and install ``twine``:
```shell
(my_env) $ pip install --upgrade twine
```
Once installed, run ``twine`` to upload all the archives under ``dist``:
```shell
$ pyenv activate my_env
(my_env) $ twine upload dist/*
```
You will be prompted for a username and password. After the command completes, that's done folks ! You can now ``pip`` your package:

```shell
(my_env) $ pip install my_awesome_package
```
