# Programme du jour

## 1. Les outils Python destinés au scientifique
* Les librairies indispensables
* Installation d'un environnement virtuel de départ

## 2. Welcome in the Python world
* Librairies et modules

## 3. Les principales structures de donnée dans Python
* Python built-in types: int, float, str, dict, list, tuple
* user defined type (objects)
* numpy arrays, pandas dataframes 


## 4. Jupyter, pas la planète avec un "y", mais les notebooks !
* Pour une présentation scientifique, mélangez code et texte facilement !
