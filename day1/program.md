# Programme du jour

## 1. Python, version système et environnements virtuels
* Pour ne plus confondre système et interface
* L'outil ``pyenv``

## 2. Git et la gestion des versions
* Framagit, parce que la décentralisation, il n'y a que ça de vrai (et parce qu'on em... Microsoft !)
* Votre compte sur Framagit
* Add, commit et push: le cycle de la vie

## 3. Comment uploader vos packages Python sur le catalogue PyPI
* Votre compte sur PyPI
* Python et le package ``setuptools``
* Utilisation de l'outil ``twine``

## 4. l'IDE (Integrated Development Environment) Pycharm
* Pour ne plus confondre interface et système
* Pycharm est la fenêtre qui vous donne accès à Python, c'est tout (et c'est déjà pas mal !)
* Vim, c'est bien, mais Pycharm, c'est l'arme ultime du scientifique développeur !
* Petit tour d'horizon des "settings" pour rendre la vie du programmeur meilleure

## 5. Les bonnes pratiques de programmation
* Le débogage, c'est la vie !
* Quand vous ne savez pas ce que cela va donner, faites un "scratch" !
